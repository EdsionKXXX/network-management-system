package com.example.bigdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BigdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BigdemoApplication.class, args);
    }

}
