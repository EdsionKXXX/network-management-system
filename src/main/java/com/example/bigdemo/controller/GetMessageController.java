package com.example.bigdemo.controller;

import com.example.bigdemo.pojo.SNMPobject;
import com.example.bigdemo.pojo.SetSNMPobject;
import com.example.bigdemo.pojo.Values;
import com.example.bigdemo.pojo.dto;
import com.example.bigdemo.sercive.GetMessageService;
import com.example.bigdemo.sercive.Trapservice;
import com.example.bigdemo.sercive.snmpService;
import com.example.bigdemo.tools.tool;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.snmp4j.CommandResponderEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Api(tags="SNMP查询接口")
@Slf4j
@RestController
@RequestMapping("/SNMP")
public class GetMessageController {
    @Autowired
    snmpService snmpService;

    @Autowired
    Trapservice trapservice;

    @Autowired
    GetMessageService getMessageService;

    @Autowired
    tool tool;


    @ApiOperation("GET操作")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/get")
    @ResponseBody
    public dto GetMessage(@RequestBody SNMPobject sobj){
        log.info("开始查询");
        List<Values> list= new ArrayList<>();
        Map<String,String> M = new HashMap<>();
        M.put("报文信息",getMessageService.getMessageByIpAndOid(sobj));
        Values values=new Values(0,M);
        list.add(values);
        dto dto= new dto(1,list);

        return dto;
    }

    @ApiOperation("GETNext操作")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getNext")
    @ResponseBody
    public dto GetNextMessage(@RequestBody SNMPobject sobj){
        log.info("开始查询");
        List<Values> list= new ArrayList<>();
        Map<String,String> M = new HashMap<>();
        M.put("报文信息",getMessageService.getNextMessageByIpAndOid(sobj));
        Values values=new Values(0,M);
        list.add(values);
        dto dto= new dto(1,list);
        return dto ;
    }











}
