package com.example.bigdemo.controller;

import com.example.bigdemo.pojo.SNMPobject;
import com.example.bigdemo.pojo.Values;
import com.example.bigdemo.pojo.dto;
import com.example.bigdemo.sercive.GetMessageService;
import com.example.bigdemo.sercive.Trapservice;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags="SNMP 硬件信息查询接口")
@Slf4j
@RestController
@RequestMapping("/SNMPhardware")
public class HardwareController {
    @Autowired
    com.example.bigdemo.sercive.snmpService snmpService;

    @Autowired
    Trapservice trapservice;

    @Autowired
    GetMessageService getMessageService;

    @Autowired
    com.example.bigdemo.tools.tool tool;
    @ApiOperation("获取cpu使用率")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getCpu")
    @ResponseBody
    public dto GetCpu(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        Values cpu = getMessageService.GetCPUMessage(ip);
        log.info("开始查询");
        List<Values> list= new ArrayList<>();
        list.add(cpu);
        dto dto= new dto(1,list);




        return dto;
    }

    @ApiOperation("获取内存信息")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getMemory")
    @ResponseBody
    public dto GetMemory(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        log.info("开始查询");
        List<Values> list = getMessageService.GetMemoryMessage(ip);
        return new dto(list.size(),list);
    }

    @ApiOperation("获取磁盘相关信息")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getDisk")
    @ResponseBody
    public dto GetDisk(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        log.info("开始查询");
        List<Values> list = getMessageService.GetDiskMessage(ip);
        return new dto(list.size(),list);
    }
}
