package com.example.bigdemo.controller;

import com.example.bigdemo.logmode.pojo.snmplogger;
import com.example.bigdemo.logmode.service.snmploggerService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags="日志管理")
@RestController
@Slf4j
@RequestMapping("/log")
public class LoggerController {
    @Autowired
    snmploggerService service;


    @ApiOperation("刷新当前日志")
    @ApiOperationSupport(author = "EDSION")
    @GetMapping("/reLog")
    @ResponseBody
    public void ReLog() {
        service.testAddLogger();
    }

    @ApiOperation("获取日志")
    @ApiOperationSupport(author = "EDSION")
    @GetMapping("/getLog")
    @ResponseBody
    public List<snmplogger> GetLog() {
        return service.list();
    }

    @ApiOperation("删除全部日志")
    @ApiOperationSupport(author = "EDSION")
    @DeleteMapping("/deleteAllLog")
    @ResponseBody
    public boolean DeleteLog() {
        service.deleteAll();
//        for (int i = list.get(1).getId(); i < list.size()+i+1; i++) {
//             service.removeById(i);
//        }
        return true;
    }
}
