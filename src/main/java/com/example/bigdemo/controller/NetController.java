package com.example.bigdemo.controller;

import com.example.bigdemo.pojo.SNMPobject;
import com.example.bigdemo.pojo.Values;
import com.example.bigdemo.pojo.dto;
import com.example.bigdemo.sercive.GetMessageService;
import com.example.bigdemo.sercive.Trapservice;
import com.example.bigdemo.sercive.snmpService;
import com.example.bigdemo.tools.tool;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags="SNMP net信息查询接口")
@Slf4j
@RestController
@RequestMapping("/SNMPnet")
public class NetController {
    @Autowired
    com.example.bigdemo.sercive.snmpService snmpService;

    @Autowired
    Trapservice trapservice;

    @Autowired
    GetMessageService getMessageService;

    @Autowired
    com.example.bigdemo.tools.tool tool;

    @ApiOperation("获取服务器TCP端口集合")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getPortTCP")
    @ResponseBody
    public dto GetPortTCP(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        log.info("开始查询");
        List<Values> list = getMessageService.GetPortMessage(ip);
        list=tool.BigList(list);
        return new dto(list.size(),list);
    }
    @ApiOperation("获取服务器UDP端口集合")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getPortUDP")
    @ResponseBody
    public dto GetPortUDP(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        log.info("开始查询");
        List<Values> list = getMessageService.GetPortMessage(ip);
        list=tool.SmallList(list);
        return new dto(list.size(),list);
    }
    @ApiOperation("获取服务器接口集合")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getInterface")
    @ResponseBody
    public dto GetInterface(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        log.info("开始查询");
        List<Values> list = getMessageService.GetInterfaceMessage(ip);
        list=tool.BigList(list);
        return new dto(list.size(),list);

    }
    @ApiOperation("获取服务器接口ip信息集合")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getInterfaceIP")
    @ResponseBody
    public dto GetInterfaceIP(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        log.info("开始查询");
        List<Values> list = getMessageService.GetInterfaceMessage(ip);
        list=tool.SmallList(list);
        return new dto(list.size(),list);

    }
    @ApiOperation("ping某个地址,只发IP，oid不用发")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getPing")
    @ResponseBody
    public String GetTrapPing(@RequestBody SNMPobject sobj)  {
        try {
            return trapservice.pingNet(sobj.getIp());
        } catch (Exception e) {
            e.printStackTrace();
            return "TimeOut";
        }
    }


    @ApiOperation("获取路由表操作，用于网络拓扑")
    @ApiOperationSupport(author = "EDSION")
    @GetMapping("/getR")
    @ResponseBody
    public dto GetRMessage(String ip) {
        SNMPobject sobj = new SNMPobject();
        sobj.setOid("1.3.6.1.2.1.4.21");
        sobj.setIp(ip);
        log.info("开始获取局域网路由表");
        List<Values> list = new ArrayList<>();
        Map<String, String> M = new HashMap<>();
        log.error(getMessageService.getNextMessageByIpAndOid(sobj));
        M.put("路由表", getMessageService.getNextMessageByIpAndOid(sobj));
        Values values = new Values(0, M);
        list.add(values);
        dto dto = new dto(1, list);
        return dto;
    }

    @ApiOperation("获取网络流量")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getNetflow")
    @ResponseBody
    public dto GetNetflow(String ip) throws InterruptedException {
        Map<String, String> flow = trapservice.Getflow(ip);
        List<Values> list = new ArrayList<>();
        Values values = new Values(0, flow);
        list.add(values);
        dto dto = new dto(1, list);
        return dto;
    }
    @ApiOperation("获取丢包率")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getLossw")
    @ResponseBody
    public String Getloss(String ip)  {

        return trapservice.Getloss(ip);
    }
//    1.3.6.1.2.1.31.1.1.1.1
    //ifInOctets 1.3.6.1.2.1.2.2.1.10
//    1.3.6.1.2.1.2.2.1.10.18
    //ifOutOctets 1.3.6.1.2.1.2.2.1.16
//    1.3.6.1.2.1.2.2.1.16.18
//    流入流量：
//    键值：ifHCInOctets
//    OID：1.3.6.1.2.1.31.1.1.1.6
//
//    流出流量：
//    键值：ifHCOutOctets
//    OID：1.3.6.1.2.1.31.1.1.1.10
}
