package com.example.bigdemo.controller;

import com.example.bigdemo.pojo.SetSNMPobject;
import com.example.bigdemo.sercive.GetMessageService;
import com.example.bigdemo.sercive.Trapservice;
import com.example.bigdemo.sercive.snmpService;
import com.example.bigdemo.tools.tool;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags="SNMP SET操作接口")
@Slf4j
@RestController
@RequestMapping("/SNMPset")
public class SetMessageController {
    @Autowired
    com.example.bigdemo.sercive.snmpService snmpService;

    @Autowired
    Trapservice trapservice;

    @Autowired
    GetMessageService getMessageService;

    @Autowired
    com.example.bigdemo.tools.tool tool;

    @ApiOperation("SET操作")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/SET")
    @ResponseBody
    public boolean Setmessage(@RequestBody SetSNMPobject ssoj){
        return snmpService.SetOIDvalue(ssoj.getSnmPobject().getIp(),ssoj.getSnmPobject().getOid(),ssoj.getValues());
    }
}
