package com.example.bigdemo.controller;

import com.example.bigdemo.pojo.SNMPobject;
import com.example.bigdemo.pojo.Values;
import com.example.bigdemo.pojo.dto;
import com.example.bigdemo.sercive.GetMessageService;
import com.example.bigdemo.sercive.Trapservice;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags="SNMP 软件查询接口")
@Slf4j
@RestController
@RequestMapping("/SNMPsoft")
public class SoftController {
    @Autowired
    com.example.bigdemo.sercive.snmpService snmpService;

    @Autowired
    Trapservice trapservice;

    @Autowired
    GetMessageService getMessageService;

    @Autowired
    com.example.bigdemo.tools.tool tool;

    @ApiOperation("获取服务器进程集合信息")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getProcess")
    @ResponseBody
    public dto GetProcess(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        log.info("开始查询");
        List<Values> list = getMessageService.GetProcessMessage(ip);
        return new dto(list.size(),list);
    }

    @ApiOperation("获取服务器系统服务集合")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getService")
    @ResponseBody
    public dto GetService(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        log.info("开始查询");
        List<Values> list = getMessageService.GetServiceMessage(ip);
        return new dto(list.size(),list);
    }


    @ApiOperation("获取服务器安装软件集合")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getSoft")
    @ResponseBody
    public dto GetSoft(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        log.info("开始查询");
        List<Values> list = getMessageService.GetSoftMessage(ip);
        list = tool.installList(list);
        return new dto(list.size(),list);
    }

    @ApiOperation("获取服务器安装过的软件集合")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getSoftBefore")
    @ResponseBody
    public dto GetSoftBefore(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        log.info("开始查询");
        List<Values> list = getMessageService.GetSoftMessage(ip);
        list = tool.delateList(list);
        return new dto(list.size(),list);
    }

}
