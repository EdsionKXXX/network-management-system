package com.example.bigdemo.controller;

import com.example.bigdemo.logmode.service.snmploggerService;
import com.example.bigdemo.pojo.SNMPobject;
import com.example.bigdemo.pojo.Values;
import com.example.bigdemo.pojo.dto;
import com.example.bigdemo.topomode.pojo.Topodto;
import com.example.bigdemo.topomode.pojo.portMessageObject;
import com.example.bigdemo.topomode.service.TopoService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags="拓扑所需数据抓取")
@RestController
@Slf4j
@RequestMapping("/topo")
public class TopoController {

    @Autowired
    TopoService topoService;

    @ApiOperation("获取端口信息")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getTopoPort")
    @ResponseBody
    public Topodto GetTopoPort(String ip){
        log.info("开始为拓扑获取端口");
        List<portMessageObject> list = topoService.GetPortMessage(ip);
        return new Topodto(list.size(),list);
    }
    @ApiOperation("获取关于ip的oid")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/getIpMessage")
    @ResponseBody
    public dto GetIpMessage(String ip){
        log.info("开始为拓扑获取关于ip的oid");
        List<Values> list = topoService.topoIpWalk(ip);
        return new dto(list.size(),list);
    }


}




