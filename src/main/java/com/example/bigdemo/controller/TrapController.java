package com.example.bigdemo.controller;

import com.example.bigdemo.pojo.SNMPobject;
import com.example.bigdemo.pojo.Trap;
import com.example.bigdemo.sercive.GetMessageService;
import com.example.bigdemo.sercive.Impl.GetMessageImpl;
import com.example.bigdemo.sercive.Impl.TrapserviceImpl;
import com.example.bigdemo.sercive.Impl.snmpServiceImpl;
import com.example.bigdemo.sercive.Trapservice;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.snmp4j.CommandResponderEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags="SNMP 故障接口")
@Slf4j
@RestController
@RequestMapping("/SNMPtrap")
public class TrapController {
    @Autowired
    com.example.bigdemo.sercive.snmpService snmpService;

    @Autowired
    Trapservice trapservice;
    @Autowired
    TrapserviceImpl trapserviceIMPL;
    @Autowired
    GetMessageService getMessageService;

    @Autowired
    com.example.bigdemo.tools.tool tool;
    @ApiOperation("获取故障信息，你写个for循环，一直获取当获取到true的时候就是故障")
    @ApiOperationSupport(author = "EDSION")
    @GetMapping("/getTrap")
    @ResponseBody
    public Trap GetTrap() throws InterruptedException {
        String c = trapserviceIMPL.GetTrapMesssage().toString();
        int xcs = Thread.activeCount();

        if (xcs==42){
            log.error("出现告警！！！！！！！！！");
            log.info(String.valueOf(trapservice.GetTrapMessage()));
            Trap trap = new Trap();
            trap.setIsError(Boolean.TRUE);
            trap.setTrapvalue(trapservice.GetTrapMessage().toString());
            return trap;


        }else {
            Trap trap = new Trap();
            trap.setIsError(Boolean.FALSE);
            trap.setTrapvalue("没有告警");
            return trap;
        }
    }




}
