package com.example.bigdemo.controller;

import com.example.bigdemo.pojo.SNMPobject;
import com.example.bigdemo.pojo.Values;
import com.example.bigdemo.pojo.dto;
import com.example.bigdemo.sercive.Walkservice;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags="SNMP walk查询接口")
@Slf4j
@RestController
@RequestMapping("/Walk")
public class WalkController {
    @Autowired
    Walkservice walkservice;


    @ApiOperation("walk操作")
    @ApiOperationSupport(author = "EDSION")
    @PostMapping("/walk")
    @ResponseBody
    public dto GetDisk(@RequestBody SNMPobject sobj){
        String ip = sobj.getIp();
        String oid =sobj.getOid();
        log.info("开始查询");
        List<Values> list = walkservice.snmpWalk(ip,oid);
        return new dto(list.size(),list);
    }
}
