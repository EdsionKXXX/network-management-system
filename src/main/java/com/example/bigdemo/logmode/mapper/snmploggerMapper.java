package com.example.bigdemo.logmode.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.bigdemo.logmode.pojo.snmplogger;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface snmploggerMapper extends BaseMapper<snmplogger> {

}
