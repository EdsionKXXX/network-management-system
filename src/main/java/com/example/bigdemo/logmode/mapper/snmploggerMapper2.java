package com.example.bigdemo.logmode.mapper;

import com.example.bigdemo.logmode.pojo.snmplogger;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

@Component
public interface snmploggerMapper2 extends PagingAndSortingRepository<snmplogger, Integer> {
   void deleteAllInBatch();
}
