package com.example.bigdemo.logmode.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@ApiModel("日志")
@AllArgsConstructor
@NoArgsConstructor
@Entity     // 作为hibernate 实体类
@Table(name = "snmplogger")       // 映射的表明
public class snmplogger implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty("主键")
    @TableId(value = "id",type = IdType.AUTO)
    private int id;

    @Column(name = "type")
    @ApiModelProperty("日志来源")
    private String type;

    @Column(name = "logger")
    @ApiModelProperty("日志内容")
    private String logger;

    @Column(name = "time")
    @ApiModelProperty("日志时间")
    private String time;
}
