package com.example.bigdemo.logmode.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.bigdemo.logmode.pojo.snmplogger;

public interface snmploggerService extends IService<snmplogger> {
    void testAddLogger();
    Iterable<snmplogger> findAll();
    void deleteAll();
//    snmplogger getLogger();
}
