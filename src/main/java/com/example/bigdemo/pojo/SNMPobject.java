package com.example.bigdemo.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
@Data
@ApiModel("SNMP操作对象")
@Component
//@AllArgsConstructor
public class SNMPobject implements Serializable {
    //ip
    @ApiModelProperty("IP")
    private String ip;

    //oid
    @ApiModelProperty("OID")
    private String oid;
}
