package com.example.bigdemo.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@ApiModel("SNMP set操作对象")
@Component
public class SetSNMPobject {

    @ApiModelProperty("snmp普通对象")
    private SNMPobject snmPobject;


    @ApiModelProperty("具体要set得值")
    private String values;
}
