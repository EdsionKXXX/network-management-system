package com.example.bigdemo.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.snmp4j.CommandResponderEvent;
import org.springframework.stereotype.Component;

@Data
@ApiModel("告警故障类")
@Component
public class Trap {
    @ApiModelProperty("是否有故障告警")
    private Boolean isError;


    @ApiModelProperty("具体告警故障内容")
    private String trapvalue;
}
