package com.example.bigdemo.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;
@Data
@ApiModel("返回具体数据的key value值")
@AllArgsConstructor
@NoArgsConstructor
public class Values {
    //num
    @ApiModelProperty("id")
    private int num;
    @ApiModelProperty("value")
    Map<String, String> value = new HashMap<String, String>();
}
