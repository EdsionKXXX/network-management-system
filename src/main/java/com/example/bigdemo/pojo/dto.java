package com.example.bigdemo.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@ApiModel("前后端数组型交互类")
@AllArgsConstructor
@NoArgsConstructor
@Repository
public class dto {
    @ApiModelProperty("数据长度")
    private int size;
    @ApiModelProperty("具体数据")
    private List<Values> data;
}
