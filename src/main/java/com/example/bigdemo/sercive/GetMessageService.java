package com.example.bigdemo.sercive;

import com.example.bigdemo.pojo.SNMPobject;
import com.example.bigdemo.pojo.Values;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public interface GetMessageService {
     String getMessageByIpAndOid(SNMPobject sobj);
     String getNextMessageByIpAndOid(SNMPobject sobj);
     //获取cpu使用率
      Values GetCPUMessage(String ip);

     //获取内存相关信息
     List<Values> GetMemoryMessage(String ip );

     //获取磁盘相关信息
     List<Values> GetDiskMessage(String ip);

     //服务器端口集合
     List<Values> GetPortMessage(String ip);

     //服务器进程集合信息
     List<Values> GetProcessMessage(String ip);

     //服务器系统服务集合
     List<Values> GetServiceMessage(String ip);

     //服务器接口集合
     List<Values> GetInterfaceMessage(String ip);

     //服务器安装软件集合
     List<Values> GetSoftMessage(String ip) ;

}
