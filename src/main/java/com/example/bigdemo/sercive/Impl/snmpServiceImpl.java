package com.example.bigdemo.sercive.Impl;

import com.example.bigdemo.pojo.Values;
import com.example.bigdemo.sercive.snmpService;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.*;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.MessageProcessingModel;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.*;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.PDUFactory;
import org.snmp4j.util.TableEvent;
import org.snmp4j.util.TableUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
@Data
public class snmpServiceImpl implements snmpService {
 Logger Setlogger = LoggerFactory.getLogger(snmpServiceImpl.class);
    private Snmp snmp = null;
    private Address agentAddrPort = null;

    public void initComm(String ip) throws IOException {
        agentAddrPort = GenericAddress.parse("udp:"+ip+"/161");
        TransportMapping transport = new DefaultUdpTransportMapping();
        snmp = new Snmp(transport);
        transport.listen();
    }

    public ResponseEvent sendPDU(PDU pdu) throws IOException {
        CommunityTarget target = new CommunityTarget();
        target.setCommunity(new OctetString("public"));
        target.setAddress(agentAddrPort);
        target.setRetries(2);
        target.setTimeout(1500);
        target.setVersion(SnmpConstants.version1);
        return snmp.send(pdu, target);
    }

    public void setPDU(String oid ,String value) throws IOException {
        PDU pdu = new PDU();
        pdu.add(new VariableBinding(new OID(oid),
                new OctetString(value) ) );
        pdu.setType(PDU.SET);
        readResponse(sendPDU(pdu));
    }

//    public void getPDU(String oid ,String value) throws IOException {
//        PDU pdu = new PDU();
//        pdu.add(new VariableBinding(new OID(oid)));
//        pdu.setType(PDU.GET);
//        readResponse(sendPDU(pdu));
//    }

    public void readResponse(ResponseEvent respEvnt) {
        if(respEvnt == null) {
            System.out.println("respEvnt == null");
        } else if(respEvnt.getResponse() == null) {
            System.out.println("respEvnt.getResponse() == null");
        } else if (respEvnt != null && respEvnt.getResponse() != null) {
            @SuppressWarnings("unchecked")
            Vector<VariableBinding> recVBs = (Vector<VariableBinding>) respEvnt.getResponse().getVariableBindings();
            for (int i = 0; i < recVBs.size(); i++) {
                VariableBinding recVB = recVBs.elementAt(i);
                System.out.println(recVB.getOid() + "   " + recVB.getVariable());
            }
        }
    }
    public  boolean SetOIDvalue(String ip,String oid ,String v) {
        try {
            snmpServiceImpl util = new snmpServiceImpl();
            util.initComm(ip);
            util.setPDU(oid,v);
            return new Boolean(true);
        } catch (IOException e) {
            e.printStackTrace();
            return new Boolean(false);
        }

    }


}
