package com.example.bigdemo.sercive;

import org.snmp4j.CommandResponderEvent;

import java.util.Map;

public interface Trapservice {
    CommandResponderEvent GetTrapMessage() throws InterruptedException;
    public String pingNet(String ip) throws Exception;
    public Map<String, String> Getflow(String ip) throws InterruptedException;
    public String Getloss(String ip);
}
