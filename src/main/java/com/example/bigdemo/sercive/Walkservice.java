package com.example.bigdemo.sercive;

import com.example.bigdemo.pojo.Values;

import java.util.List;

public interface Walkservice {
    List<Values> snmpWalk(String ip, String oid);
}
