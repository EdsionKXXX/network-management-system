package com.example.bigdemo.tools;

import org.snmp4j.CommandResponderEvent;

import java.util.EventListener;

public interface CommandResponder2 extends EventListener {
    String processPdu(CommandResponderEvent respEvnt);
}
