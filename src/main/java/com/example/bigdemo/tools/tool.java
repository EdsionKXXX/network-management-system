package com.example.bigdemo.tools;

import com.example.bigdemo.pojo.Values;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Slf4j
@Component
public class tool {
    public List<Values> BigList(List<Values> list){
        ArrayList<Values> big = new ArrayList<>();
        int j =0;
        for (Values values :list){
            if (values.getNum()>9999/2){
                values.setNum(j);
                big.add(values);
                j++;
            }
        }
        return big;
    }
    public List<Values> SmallList(List<Values> list){
        ArrayList<Values> small = new ArrayList<>();
        int j =0;
        for (Values values :list){
            if (values.getNum()<=9999/2){
                values.setNum(j);
                small.add(values);
                j++;
            }
        }
        return small;
    }
    public List<Values> delateList(List<Values> list){
        ArrayList<Values> d = new ArrayList<>();
        int j =0;
        for (Values values :list){

            if (values.getValue().get("软件名称")==null){

                values.setNum(j);
                values.getValue().put("软件名称","已删除软件");
                d.add(values);
                j++;
            }

        }
        return d;
    }
    public List<Values> installList(List<Values> list){
        ArrayList<Values> i = new ArrayList<>();
        int j =0;
        for (Values values :list){
            if (values.getValue().get("软件名称")!=null){

                values.setNum(j);
                i.add(values);
                j++;
            }
        }
        return i;
    }


}


