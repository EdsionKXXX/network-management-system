package com.example.bigdemo.topomode.pojo;

import com.example.bigdemo.pojo.Values;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Data
@ApiModel("前后端数组型交互类")
@AllArgsConstructor
@NoArgsConstructor
@Repository
public class Topodto {
    @ApiModelProperty("数据长度")
    private int size;
    @ApiModelProperty("具体数据")
    private List<portMessageObject> data;
}
