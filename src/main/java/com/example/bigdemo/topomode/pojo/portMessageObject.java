package com.example.bigdemo.topomode.pojo;

import com.example.bigdemo.pojo.Values;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Data
@ApiModel("端口信息类")
@AllArgsConstructor
@NoArgsConstructor
@Repository
public class portMessageObject {
//     "1.3.6.1.2.1.2.2.1.2",  //descr
//             "1.3.6.1.2.1.2.2.1.3",  //type
//             "1.3.6.1.2.1.2.2.1.5",  //speed
//             "1.3.6.1.2.1.2.2.1.6",  //mac
//             "1.3.6.1.2.1.2.2.1.7",  //adminStatus
//             "1.3.6.1.2.1.2.2.1.8",  //operStatus
//
//             "1.3.6.1.2.1.2.2.1.10", //inOctets
//             "1.3.6.1.2.1.2.2.1.16", //outOctets
//             "1.3.6.1.2.1.2.2.1.14", //inError
//             "1.3.6.1.2.1.2.2.1.20", //outError
//             "1.3.6.1.2.1.2.2.1.13", //inDiscard
//             "1.3.6.1.2.1.2.2.1.19", //outDiscard
//             "1.3.6.1.2.1.2.2.1.11", //inUcastPkts
//             "1.3.6.1.2.1.2.2.1.17", //outUcastPkts
//             "1.3.6.1.2.1.2.2.1.12", //inNUcastPkts
//             "1.3.6.1.2.1.2.2.1.18"};//outNUcastPkts
    @ApiModelProperty("端口描述")
    private String ifdescr;
    @ApiModelProperty("端口类型")
    private String type;
    @ApiModelProperty("状态")
    private String ifoperStatus;

}
