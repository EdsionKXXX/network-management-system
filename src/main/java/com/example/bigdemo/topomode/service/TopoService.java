package com.example.bigdemo.topomode.service;

import com.example.bigdemo.pojo.Values;
import com.example.bigdemo.topomode.pojo.portMessageObject;

import java.util.List;

public interface TopoService {
     List<portMessageObject> GetPortMessage(String ip);
     List<Values> topoIpWalk(String ip);

}
