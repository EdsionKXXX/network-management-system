package com.example.bigdemo.trapMode;


import org.snmp4j.CommandResponderEvent;



/**
 * 处理类
 *
 */
public class SnmpTrapHandler implements Runnable{

    @Override
    public void run() {
        while(true){
            try {
                CommandResponderEvent resEvent=QueueCenter.getRespEvntMsg().take();

                System.out.println("event:"+resEvent);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}