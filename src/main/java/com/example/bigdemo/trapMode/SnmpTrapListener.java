package com.example.bigdemo.trapMode;


import org.snmp4j.CommandResponder;
import org.snmp4j.CommandResponderEvent;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.MPv1;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.mp.MPv3;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.MultiThreadedMessageDispatcher;
import org.snmp4j.util.ThreadPool;


/**
 * 监听类
 *
 */
public class SnmpTrapListener extends AbstListener implements CommandResponder{

    private String ip; //本地IP
    private String port; //监听端口
    private Address listenAddress; //地址信息
    private ThreadPool threadPool;
    private MultiThreadedMessageDispatcher dispatcher;
    @Override
    public void init() {
        try{
            threadPool=ThreadPool.create("Trap", 2);
            dispatcher=new MultiThreadedMessageDispatcher(threadPool, new MessageDispatcherImpl());
            listenAddress = GenericAddress.parse(System.getProperty(
                    "snmp4j.listenAddress", "udp:" + ip + "/" + port));
            TransportMapping transport;
            // 对TCP与UDP协议进行处理
            if (listenAddress instanceof UdpAddress) {
                transport = new DefaultUdpTransportMapping(
                        (UdpAddress) listenAddress);
            } else {
                transport = new DefaultTcpTransportMapping(
                        (TcpAddress) listenAddress);
            }
            Snmp snmp = new Snmp(dispatcher, transport);
            snmp.getMessageDispatcher().addMessageProcessingModel(new MPv1());
            snmp.getMessageDispatcher().addMessageProcessingModel(new MPv2c());
            snmp.getMessageDispatcher().addMessageProcessingModel(new MPv3());
            USM usm = new USM(SecurityProtocols.getInstance(), new OctetString(
                    MPv3.createLocalEngineID()), 0);
            SecurityModels.getInstance().addSecurityModel(usm);
            snmp.listen();
            snmp.addCommandResponder(this);

            System.out.println("启动监听成功");
        }catch (Exception e){
            System.out.println("snmp 初始化失败");
            e.printStackTrace();
        }
    }


    //此方法为CommandResponder 接口实现方法用于监听后的处理方法,将接收到的trap信息入队
    @Override
    public void processPdu(CommandResponderEvent CREvent) {
        System.out.println("in processPdu");
        this.putMessage2Queue(CREvent);
    }


    public String getIp() {
        return ip;
    }


    public void setIp(String ip) {
        this.ip = ip;
    }


    public String getPort() {
        return port;
    }


    public void setPort(String port) {
        this.port = port;
    }


    public Address getListenAddress() {
        return listenAddress;
    }


    public void setListenAddress(Address listenAddress) {
        this.listenAddress = listenAddress;
    }



}
