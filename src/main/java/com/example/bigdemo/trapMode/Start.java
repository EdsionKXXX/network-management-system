package com.example.bigdemo.trapMode;

import org.snmp4j.CommandResponderEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;


/**
 * 启动类
 */
@Component
public class Start {
    public static ApplicationContext fsxac;
    public CommandResponderEvent startTrap() throws InterruptedException {
        fsxac = new ClassPathXmlApplicationContext("applicationContext.xml");

        //启动处理线程
        new Thread(new SnmpTrapHandler()).start();
        //启动监听线程
        new Thread((SnmpTrapListener)fsxac.getBean("trapListener")).start();


        QueueCenter Q = new QueueCenter();
        return Q.outputQ();
    }
}

